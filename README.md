# sample-process

Sample BPM process

## Build & setup 

The project relies on some libraries not available in the central maven repo. Therefore you need to use custom maven settings located in this project.
From command line you can build the project like this:  

`mvn clean package -s settings.xml -P docker`

After build, you end up with a docker container in local repo: `cu04/sample-process:latest`

## Develop processes with jBPM env

1. run jBPM all-in-one image:   
`docker run --rm -p 8080:8080 -p 8001:8001 -d --name jbpm-server-full jboss/jbpm-server-full:latest`
2. login to business central(wbadmin/wbadmin) and create a new project named `sample`
3. add second git origin to the process repo:  
`git remote add jbpm http://localhost:8080/business-central/git/MySpace/sample`
4. push to jbpm:  
`git push jbpm master -f`
5. open the process in business central under your project `sample` and edit it
6. when you are done, do not forget to pull the changes back to local repo:   
`git pull jbpm master`
